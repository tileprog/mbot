BOARD_TAG			= uno
MONITOR_PORT		= /dev/ttyUSB0
MONITOR_BAUDRATE	= -b 9600
MONITOR_CMD			= picocom -c --imap lfcrlf
ARDUINO_LIBS		= Makeblock Wire Reactduino

USE_RGB_LINE_FOLLOWER_ARRAY=0

CPPFLAGS			= -Wno-unused-variable 

legacy: 
	mkdir -p build-$(BOARD_TAG)/legacy/
	echo "#define USE_RGB_LINE_FOLLOWER_ARRAY 0" > config.h
	cp -a lib/lfa/MeLineFollowerArray.* .
	OBJDIR=build-$(BOARD_TAG)/legacy/ $(MAKE) all
	rm MeLineFollowerArray.*

rgb: 
	mkdir -p build-$(BOARD_TAG)/rgb/
	echo "#define USE_RGB_LINE_FOLLOWER_ARRAY 1" > config.h
	cp -a lib/rgblf/MeRGBLineFollower.* .
	OBJDIR=build-$(BOARD_TAG)/rgb/ $(MAKE) all
	rm MeRGBLineFollower.*

include $(ARDMK_DIR)/Arduino.mk
