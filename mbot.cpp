#include "mbot.h"

#if USE_RGB_LINE_FOLLOWER_ARRAY==1
    #warning use RGB line follower array
#else
    #warning use legacy line follower array
#endif


MBot::MBot() {
    _motor_left = MeDCMotor(M1);
    _motor_right = MeDCMotor(M2);
    _leds = MeRGBLed(PORT_7, 2);
    _us_sensor = MeUltrasonicSensor(PORT_3);
    _lf_sensor = MeLineFollower(PORT_2);

#if USE_RGB_LINE_FOLLOWER_ARRAY==1
    _lfa_sensor = MeRGBLineFollower(PORT_1);
#else
    _lfa_sensor = MeLineFollowerArray(PORT_1);
#endif

    _max_dist = DEFAULT_OBSTABLE_MAX_DIST;

    _stop_at = 0;
    _is_following_line = false;
}

MBot::~MBot() {
    stop();
}

void MBot::reset() {
    _motor_left.stop();
    _motor_right.stop();
    _speed = 0;
    _is_following_line = false;
    _max_dist = DEFAULT_OBSTABLE_MAX_DIST;

    _leds.setColor(0, 0, 0);
    _leds.show();
}

void MBot::update(void) {
    _dist = (unsigned int)_us_sensor.distanceCm(_max_dist);
    if (_dist == _max_dist) _dist = NO_OBSTACLE;

    _lf_sensor_state = _lf_sensor.readSensors();

#if USE_RGB_LINE_FOLLOWER_ARRAY==1
    // the RGB line sensor value seems to be usable as is
    _lfa_sensor_state = _lfa_sensor.getPositionState();
#else
    // we must reverse the bit sequence of the line array, 
    // since it does not follows the same convention 
    // as the simple line follower 
    uint8_t raw_value = _lfa_sensor.get_value();
    uint8_t reversed_value = 0;
    
    for (uint8_t i = 0; i < 6; i++) {
        if (raw_value & 1) {
            reversed_value |= 1;
        }
        raw_value >>= 1;
        reversed_value <<= 1;
    }
    _lfa_sensor_state = reversed_value >> 1;
#endif

    if (is_moving()) {
        if (_stop_at) {
            if (millis() >= _stop_at) {
                stop();
                _stop_at = 0;
            }
        }

        // test motion again since the state could have been changed 
        // by timed stop handling just above
        if (is_moving() && _is_following_line) {
            switch(_lf_sensor_state) {
                case S1_IN_S2_IN:
                    drive_straight(_speed);
                    break;
                case S1_IN_S2_OUT:
                    _motor_left.run(0);
                    _motor_right.run(_speed);
                    break;
                case S1_OUT_S2_IN:
                    _motor_left.run(-_speed);
                    _motor_right.run(0);
                    break;
                case S1_OUT_S2_OUT:
                    stop();
                    break;

            }
        }
    }
}

void MBot::drive_straight(int speed) {
    _motor_left.run(-speed);
    _motor_right.run(speed);

    _speed = speed;
}

void MBot::drive_straight(int speed, int msecs) {
    drive_straight(speed);
    _stop_at = millis() + msecs;
}

void MBot::spin(int speed, int dir) {
    if (dir > 0) {
        _motor_left.run(speed);
        _motor_right.run(speed);
    } else if (dir < 0) {
        _motor_left.run(-speed);
        _motor_right.run(-speed);
    } else {
        return;
    }

    _speed = speed;
}

void MBot::spin(int speed, int dir, int msecs) {
    spin(speed, dir);
    _stop_at = millis() + msecs;
}

void MBot::forward(int speed) {
    drive_straight(speed);
}

void MBot::forward(int speed, int msecs) {
    drive_straight(speed, msecs);
}

void MBot::backward(int speed) {
    drive_straight(-speed);
}

void MBot::backward(int speed, int msecs) {
    drive_straight(-speed, msecs);
}

void MBot::spin_right(int speed) {
    spin(speed, DIR_RIGHT);
}

void MBot::spin_right(int speed, int msecs) {
    spin(speed, DIR_RIGHT, msecs);
}

void MBot::spin_left(int speed) {
    spin(speed, DIR_LEFT);
}

void MBot::spin_left(int speed, int msecs) {
    spin(speed, DIR_LEFT, msecs);
}

void MBot::stop(void) {
    _motor_left.stop();
    _motor_right.stop();

    _speed = 0;
    _is_following_line = false;
}

void MBot::follow_line(int speed) {
    drive_straight(speed);
    _is_following_line = true;
}

void MBot::set_leds(uint8_t r, uint8_t g, uint8_t b) {
    _leds.setColor(r, g, b);
    _leds.show();
}

void MBot::set_led(uint8_t which, uint8_t r, uint8_t g, uint8_t b, bool show) {
    _leds.setColorAt(which, r, g, b);
    if (show) {
        _leds.show();
    }
}

void MBot::set_led(uint8_t which, uint8_t r, uint8_t g, uint8_t b) {
    set_led(which, r, g, b, true);
}
