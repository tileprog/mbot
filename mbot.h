#ifndef __mbot_h__
#define __mbot_h__

/*
   Remember to include type definition headers here only, and NOT 
   board ones (such as MeMCore.h) as a shortcut. 

   First, modules are not supposed to be board dependant in order to be 
   portable. 

   Second, if the board include is used in more than one place in the project,
   it will result in duplicated declarations of the mePort global variable
   (port IOs mapping), and thus link edit fatal errors.
*/

#include <MeDCMotor.h>
#include <MeRGBLed.h>
#include <MeUltrasonicSensor.h>
#include <MeLineFollower.h>
#include <MeBuzzer.h>

#include "config.h"
#if USE_RGB_LINE_FOLLOWER_ARRAY==1
    #include "lib/rgblf/MeRGBLineFollower.h"
#else
    #include "lib/lfa/MeLineFollowerArray.h"
#endif

#define NO_OBSTACLE        0x7fff 

#define DIR_LEFT            1
#define DIR_CCW             1
#define DIR_TRIG            1
#define DIR_RIGHT           -1
#define DIR_CW              -1
#define DIR_ANTI_TRIG       -1

#define LED_RIGHT           0
#define LED_LEFT            1

#define DEFAULT_OBSTABLE_MAX_DIST    15

class MBot {
        MeDCMotor _motor_left;
        MeDCMotor _motor_right;
        MeRGBLed _leds;
        MeUltrasonicSensor _us_sensor;
        MeLineFollower _lf_sensor;
#if USE_RGB_LINE_FOLLOWER_ARRAY==1
        MeRGBLineFollower _lfa_sensor;
#else
        MeLineFollowerArray _lfa_sensor;
#endif
        MeBuzzer _buzzer;

        unsigned long _stop_at;
        int _speed;
        unsigned int _dist;
        unsigned int _max_dist;
        bool _is_following_line;
        uint8_t _lf_sensor_state;
        uint8_t _lfa_sensor_state;

    public:
        MBot() ;
        ~MBot() ;

        inline void set_max_dist(unsigned int);
        void reset() ;
        void update(void) ;

        void drive_straight(int speed) ;
        void drive_straight(int speed, int msecs) ;
        void spin(int speed, int dir) ;
        void spin(int speed, int dir, int msecs) ;
        void forward(int speed) ;
        void forward(int speed, int msecs) ;
        void backward(int speed) ;
        void backward(int speed, int msecs) ;
        void spin_right(int speed) ;
        void spin_right(int speed, int msecs) ;
        void spin_left(int speed) ;
        void spin_left(int speed, int msecs) ;
        void stop(void) ;
        void follow_line(int speed) ;

        inline bool is_moving(void) ;
        inline int speed() ;
        inline bool is_following_line() ;

        void set_leds(uint8_t r, uint8_t g, uint8_t b) ;
        void set_led(uint8_t which, uint8_t r, uint8_t g, uint8_t b, bool show);
        void set_led(uint8_t which, uint8_t r, uint8_t g, uint8_t b) ;
        inline void leds_off();
        inline void led_off(uint8_t which);

        inline unsigned int obstacle_distance() ;
        inline bool obstacle_detected() ;

        inline uint8_t lf_sensor_state();
        inline uint8_t lfa_sensor_state();

        inline void tone(uint16_t frequency, uint32_t duration=0);
        inline void noTone();
};

inline void MBot::leds_off() {
    set_leds(0, 0, 0);
}

inline void MBot::led_off(uint8_t which) {
    set_led(which, 0, 0, 0);
}

inline void MBot::set_max_dist(unsigned int dist) {
    _max_dist = dist;
}

inline bool MBot::is_moving(void) {
    return _speed != 0;
}

inline int MBot::speed() {
    return _speed;
}

inline bool MBot::is_following_line() {
    return _is_following_line;
}

inline unsigned int MBot::obstacle_distance() {
    return _dist;
}

inline bool MBot::obstacle_detected() {
    return _dist != NO_OBSTACLE ;
}

inline uint8_t MBot::lf_sensor_state() {
    return _lf_sensor_state;
}

inline uint8_t MBot::lfa_sensor_state() {
    return _lfa_sensor_state;
}

inline void MBot::tone(uint16_t frequency, uint32_t duration) {
    _buzzer.tone(frequency, duration);
}

inline void MBot::noTone() {
    _buzzer.noTone();
}

#endif
