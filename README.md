# TaxiBot robot firmware <!-- omit in toc -->

- [Overview](#overview)
- [Forewords](#forewords)
- [Dependencies](#dependencies)
  - [Reactduino library](#reactduino-library)
  - [Arduino-mk](#arduino-mk)
- [Build commands](#build-commands)

## Overview

This is the firmware of the mBot used by the TaxiBot (aka TileProg) demonstrator.

The mBot is equiped with an Arduino type control board named **MCore**. In addition to the front default
2 sensors line follower, a 6 sensors line follower array has been added at the wheels axle position
to detect crossing lines.

Since having the integrated Bluetooth work reliabily with a RaspberryPi, the module has been removed
and an **XBee** module has been instead. I is connected to the serial communication signals
of the radio expansion headers of the MCOre board.

## Forewords

This project is not a classical `*.ino` based one, intended to work with the
Arduino default editing tool *(sorry, I can't call it an IDE, despite the way
it is described)*.

My experience has shown that it exposes you to
brain damages when using it for something more complicated than
just blinking a LED :)

So I just dumped it in favor of the traditionnal C/C++ and Makefile workflow
and real developpers tools.

**My advice** : make you a favor and switch to stuff such as VSCode
(source: <https://code.visualstudio.com/)> for instance. I do not usually have any particular
affinity with Microsoft products, but I must admit that this one is rather effective,
and moreover, it's free and multi-platform.

## Dependencies

The project depends on the following libraries and tool.

### Reactduino library

Source: <https://github.com/Reactduino/Reactduino>

It lets you write event driven applications without headaches.
Do not forget that any real world Arduino application **IS** event driven by nature, apart
toy applications writen with calls to the `delay()` function.

**My advice** : **never use** the `delay()` function, apart if you want your code to behave
erratically just because it misses events occuring while waiting.

### Arduino-mk

Source: <https://github.com/sudar/Arduino-Makefile>

It brings you the full power of Makefile, but without having to write a Makfile. Have o look at this
project Makefile to measure the benefits.

## Build commands

Refer to Arduino-mk documentation for details. Here after are the common targets.

| Action | Command |
| --- | --- |
| compile sources | `make` |
| upload binary | `make upload` |
| cleanup compilation results | `make clean` |
