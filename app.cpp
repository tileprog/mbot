/*
 * Tileprog robot controller firmware
 * 
 * This firmware handles commands received on the serial link and controls the
 * robot motions over the guiding grid. It also detects obstacles and report
 * the current state.
 * 
 * Available motion related commands are :
 * - 'F' : move forward to the next crossing
 * - 'R' : turn right to the next line
 * - 'L' : turn left to the next line
 * - 'S' : stop the ongoing move if any
 * 
 * Sequences of elementary moves can be provided, prefixed by a '$' sign/
 * ex: $FFRFL
 * 
 * Other action commands are available :
 * - '!' : panic reset of the application
 * 
 * Various information such as the distance to the obstacle if any, the current robot
 * state, the line following sensors reading,... can be queried. The corresponding commands
 * start with a question mark (?') character, followed by the information identification
 * (ex: '?D' for getting the distance to the obstacle).
 * 
 * Currently supported queries are :
 * - 'D' : returns the distance (in cm) to the obstacle, if any
 * - 'O' : returns is an obstacle is detected or not
 * - 'M' : returns 1 if the robot is currently moving or 0 if it is stopped
 * - 'X' : returns 1 if the robot is currently stopped at a crossing
 * - 'F' : returns the front line follower reading (bit pattern as an int)
 * - 'A' : returns the axle line follower reading (bit pattern as an int)
 * - 'S' : returns the current state
 * - '#' : activates sensors streaming 
 * - '!' : disables sensors streaming
 * 
 * Responses start with a '<' char ans are composed of 2 or 3 blank separated fields, 
 * which content is:
 * - field 1 : command status: 
 *   + '<OK'  : command execution success 
 *   + '<INV' : invalid command
 * - field 2 : processed command, as received
 * - field 3 : optional command result (f.i. value for query commands)
 *  
 * Events are sent by the robot to reflect state changes. The packets start with a '!'
 * char, followed by the event type char:
 * - 'S' : sequence started
 * - 'F' : forward motion started
 * - 'R' : right turn started
 * - 'L' : left turn started
 * - 'X' : crossing reached
 * - 'T' : track lost
 * - 'O' : obstacle detected
 * - 'A' : arrived at destination (i.e. sequence complete)
 *  
 * When using Bluetooth on a RPi, use the following command to 
 * create a serial link over the BT connection to the robot : 
 * 
 *      $ sudo rfcomm bind /dev/rfcomm0 <mBot BT addr> 1
 *      Connected /dev/rfcomm0 to <mBot BT addr> on channel 1
 * 
 * (mentionning '1' for the channel is not required since defaulted to this value if omitted)
 * 
 * In order to stay reactive to commands and events, the firmware is implemented 
 * in asynchronous mode, based on the Reactduino library (https://github.com/Reactduino/Reactduino).
 * The robot behaviour control uses a FSM to manage its various states. 
 * Remember : blocking delays are evil :)
 */

#include "MeMCore.h"

#include <Reactduino.h>
#include "mbot.h"       // our robot model, based on a mBot

#define DEBUG               0

#define SERIAL_BAUDS        9600

// communication buffers 

#define RX_BUFFER_SIZE      256
#define TX_BUFFER_SIZE      32

static char rx_buffer[RX_BUFFER_SIZE];                 
static char tx_buffer[TX_BUFFER_SIZE];                  

static uint8_t in_buffer = 0;                           // chars already recieved

// commands and replies definitions

#define COMMAND_MAX_LEN     (RX_BUFFER_SIZE - 1)
#define REPLY_MAX_LEN       (TX_BUFFER_SIZE - 1)

static char *cmde_p = rx_buffer;                        // pointer for command parsing (available at start of Rx buffer)

static char move_seq[COMMAND_MAX_LEN];                  // no need to account for the null here, since the sequence is always shorter than the command
static char *move_seq_pc = NULL;                        // current move in a sequence

#define executing_sequence (move_seq_pc != NULL)

#define result_value (tx_buffer + 1)                    // the result (if any) starts after the reply opening character

// User signals (not used for the moment)
#define SIG_OBSTACLE        1
#define SIG_AT_CROSSING     2

// The robot FSM states
enum State {
    ready,
    following_line,
    track_lost,
    turning,
    maybe_crossing,
    at_crossing,
    leaving_crossing,
    turning_stage_1,
    turning_stage_2,
    blocked,
    panic,
};

State state = ready;                                    // the current FSM state

static MBot robot;                                      // our robot

static unsigned int obstacle_distance ;                 // distance to the osbtacle, if any

/* 
 * Hereafter line following sensors state are bit patterns, in which each position
 * represents the state of the corresponding sensor. 0 means "over a black area" (no
 * light reflected) while 1 means over a white area.
 */
static uint8_t lf_state ;                               // front line following state
static uint8_t lfa_state ;                              // axle line follower array state
static uint8_t crossing_repeats;                        // consecutive crossing detection count (for debouncing)

// how many consecutive detections of a crossing line will be waited for before assessing we reached it
#define CROSSING_CONFIRMATIONS              10

// A couple of forward declarations to simplify code writing

static bool handle_action_command(char cmde_char);
static bool handle_query_command(char cmde_char);

// Reactions related

static reaction crossing_leaving_delay_reaction = INVALID_REACTION;

// Feedback sending

#define EVENT_STARTING          'S'
#define EVENT_AT_CROSSING       'X'
#define EVENT_MOVING_FORWARD    'F'
#define EVENT_TURNING_RIGHT     'R'
#define EVENT_TURNING_LEFT      'L'
#define EVENT_ARRIVED           'A'
#define EVENT_TRACK_LOST        'T'
#define EVENT_OBSTACLE          'O'

static void send_event_to_controller(char event) {
    Serial.write('!');
    Serial.write(event);
    Serial.write('\n');
    Serial.flush();
}

static bool sensors_streaming_enabled = false;

static void stream_sensors_to_controller() {
    if (sensors_streaming_enabled) {
        Serial.write("!# ");
        Serial.print(obstacle_distance, DEC);
        Serial.write(' ');
        Serial.print(lf_state, DEC);
        Serial.write(' ');
        Serial.print(lfa_state, DEC);
        Serial.write('\n');
        Serial.flush();
    }
}

/* ******************************************************
 * Internal functions
 * ****************************************************** */

/*
 * Emit a sound signaling that an obstacle is detected.
 */ 
static void emit_obstacle_sound() {
    robot.tone(587, 100);
    robot.tone(659, 100);
    robot.tone(523, 100);
}

/*
 * Emit a sound to notify an error.
 */
static void emit_error_sound() {
    robot.tone(659, 100);
    robot.tone(587, 100);
    robot.tone(659, 100);
}

static void _tone_and_led(int ms, int r, int g, int b) {
    robot.set_leds(r, g, b);
    robot.tone(440, ms);
    robot.leds_off();
    delay(100);
}

/*
 * Notify that we are ready by sounding and blinking accordingly
 */
static void notify_readiness() {
    for (uint8_t i=0 ; i<2; i++) {
        _tone_and_led(50, 0, 100, 0);
    }
}

/*
 * Notify a panic by sounding and blinking accordingly
 */
static void notify_panic() {
    for (uint8_t i=0 ; i<3; i++) {
        _tone_and_led(50, 100, 0, 0);
    }
    delay(100);
    for (uint8_t i=0 ; i<3; i++) {
        _tone_and_led(250, 100, 0, 0);
    }
    delay(100);
    for (uint8_t i=0 ; i<3; i++) {
        _tone_and_led(50, 100, 0, 0);
    }
}

/*
 * Notify we arrived at destination
 */
static uint16_t song[][2] = {
    {293, 150},
    {293, 150},
    {293, 150},
    {392, 900}
    // {587, 900},
    // {523, 150},
    // {494, 150},
    // {440, 150},
    // {784, 900},
    // {587, 900},
    // {523, 150},
    // {494, 150},
    // {440, 150},
    // {784, 900},
    // {587, 900},
    // {523, 150},
    // {494, 150},
    // {523, 150},
    // {440, 900}
};

#define SONG_LEN    (sizeof(song)  / sizeof(uint16_t) / 2)

static void notify_arrival() {
    robot.set_leds(100, 100, 100);

    for (uint8_t i = 0; i < SONG_LEN ; i++) {
        robot.tone(song[i][0], song[i][1]);
    }

    robot.leds_off();
}

/*
 * Reset the command receive buffer
 */
static void clear_command() {
    cmde_p = rx_buffer; 
    in_buffer = 0;
}

/*
 * Reset the move sequence
 */
static void reset_move_sequence() {
    move_seq_pc = NULL;
    memset(move_seq, 0, sizeof(move_seq));
}

/*
 * Update the state of the robot by reading its sensors 
 * and caching their value for efficiency.
 */
 static void read_robot_sensors() {
    robot.update();

    obstacle_distance = robot.obstacle_distance();
    lf_state = robot.lf_sensor_state();
    lfa_state = robot.lfa_sensor_state();
}

/*
 * Update application state
 * 
 * Controls the FSM transition, based on environment changes and
 * reveived command.
 */
static void update_application_state() {
    read_robot_sensors();

    switch (state) {
        case following_line:
        case leaving_crossing:
        case maybe_crossing:
            if (obstacle_distance < 5) {
                // an obstacle is detected while driving along a line
                // => stop, report the event and transition to 'blocked' state
                robot.stop();
                // abort running sequence if any
                reset_move_sequence();

                robot.set_leds(100, 0, 0);
                emit_obstacle_sound();
                
                state = blocked;

                send_event_to_controller(EVENT_OBSTACLE);
                return;
            }

            if (lf_state == 0b11) {
                // the front LF does not see the line anymore
                // => stop, report the situation and switch to 'track_lost' state
                robot.stop();
                // abort running sequence if any
                reset_move_sequence();

                robot.set_leds(100, 0, 0);

                state = track_lost;

                send_event_to_controller(EVENT_TRACK_LOST);
                return;
            }

            if (lfa_state == 0) {
                // all sensors of the axle array see a black line
                if (state != leaving_crossing) {   
                    // if we are not leaving a crossing, maybe we have reached the next one
                    // (we don't take this for granted yet, but will wait for this state to be confirmed
                    // by consecutive detections)
                    switch (state) {
                        case following_line:
                            // first time we see the line => enter the confirmation state but don't stop yet
                            state = maybe_crossing;
                            robot.set_leds(100, 0, 100);
                            crossing_repeats = 0;  
                            break;
                        case maybe_crossing:
                            // if we got enough confirmations, stop now and say we have reached the crossing
                            if (++crossing_repeats == CROSSING_CONFIRMATIONS) {
                                robot.stop();
                                robot.leds_off();
                                send_event_to_controller(EVENT_AT_CROSSING);
                                state = at_crossing;
                            }
                            return;
                        default:
                            break;
                    }
                }

            } else {
                if (state == maybe_crossing) {
                    // false crossing detection => back to normal following line state
                    state = following_line;
                    robot.set_leds(0, 100, 0);
                }
            }
            break;

        case turning_stage_1:
            if (lf_state == 0b11) {     
                // the front LF is no more above the line while turning to the next line
                // => switch to next phase of the turn (i.e. waiting for the target line to be reached)
                state = turning_stage_2;
            }
            break;
        case turning_stage_2:
            if (lf_state == 0) {        
                // the front LF is more above the line while looking to the next line
                // => stop and switch to the 'at_crossing' state
                robot.stop();
                robot.leds_off();

                send_event_to_controller(EVENT_AT_CROSSING);
                state = at_crossing;
            }
            break;

        default:
            break;
    }

    // if we have reached a crossing while playing a sequence, advance to the next step if any
    if (state == at_crossing && executing_sequence) {
#if DEBUG
        Serial.println(F("# --- update_application_state"));

        Serial.print(F("# - rx_buffer_addr=0x"));
        Serial.println((uint16_t)rx_buffer, HEX);
        
        Serial.print(F("# - rx_buffer="));
        Serial.println(rx_buffer);

        Serial.print(F("# - move_seq_addr=0x"));
        Serial.println((uint16_t)move_seq, HEX);

        Serial.print(F("# - move_seq="));
        Serial.println(move_seq);

        Serial.print(F("# - move_seq_pc=0x"));
        Serial.println((uint16_t)move_seq_pc, HEX);

        Serial.println(F("# ---"));
#endif

        if (*++move_seq_pc) {
            // there is more moves to do => handle next one
            handle_action_command(*move_seq_pc);
        } else {
            // we have reached the end of the sequence => exit the sequence playing mode
            move_seq_pc = NULL;

            // and tell the world we are did it
            send_event_to_controller(EVENT_ARRIVED);

            notify_arrival();

#if DEBUG
            Serial.println(F("# update_application_state> sequence complete"));
#endif
        }        
    }
}

static void activate_line_following() {
    state = following_line;
    robot.set_leds(0, 100, 0);

    // get rid of the delay task if any to avoid memory leak
    if (crossing_leaving_delay_reaction != INVALID_REACTION) {
        app.free(crossing_leaving_delay_reaction);
        crossing_leaving_delay_reaction = INVALID_REACTION;
    }
}

/*
 * Return the state matching the current robot position on the grid.
 */
static State state_for_current_position() {
    if (lfa_state == 0 && lf_state == 0) {
        // all axle LFA sensors are above a black line, and same for the front LD
        return at_crossing;

    } else if (lf_state == 0) {
        // front LF is on the line
        return ready;

    } else {
        // I've not a f**king idea where I am
        return panic;
    }
}


/*
 * Reset the application 
 */
static void app_reset() {
    sensors_streaming_enabled = false;

    robot.reset();

    clear_command();
    reset_move_sequence();

    read_robot_sensors();

    // set the FSM state based on the robot position on the grid
    state = state_for_current_position();

    // show our mood
    if (state != panic) {
        notify_readiness();
    } else {
        notify_panic();
    }
}

/*
 * Handle a query type command.
 * 
 * Queries are commands starting with a question mark and followed by a single character
 * identifying the queried information. They are used to return the value of a sensor 
 * or the state of the robot.
 * 
 * Supported query identifiers are:
 * - 'D' : returns the distance (in cm) to the obstacle, if any
 * - 'O' : returns is an obstacle is detected or not
 * - 'M' : returns 1 if the robot is currently moving or 0 if it is stopped
 * - 'X' : returns 1 if the robot is currently stopped at a crossing
 * - 'F' : returns the front line follower reading (bit pattern as an int)
 * - 'A' : returns the axle line follower reading (bit pattern as an int)
 * - 'S' : returns the current state
 * - '#' : activates sensors streaming 
 * - '!' : disables sensors streaming
 */
static bool handle_query_command(char cmde_char) {
    switch (cmde_char) {
        case 'D':
            itoa(robot.obstacle_distance(), result_value, 10);
            break;

        case 'O':
            strcpy(result_value, robot.obstacle_detected() ? "1" : "0");
            break;

        case 'M':
            strcpy(result_value, robot.is_moving() ? "1" : "0");
            break;

        case 'F':
            itoa(robot.lf_sensor_state(), result_value, 10);
            break;

        case 'A':
            itoa(robot.lfa_sensor_state(), result_value, 10);
            break;

        case 'X':
            strcpy(result_value, state_for_current_position() == at_crossing ? "1" : "0");
            break;

        case 'S':
            itoa(state, result_value, 10);
            break;

        case '#':
            sensors_streaming_enabled = true;
            break;

        case '!':
            sensors_streaming_enabled = false;
            break;

        default:
            return false;
    }

    return true;
}

/*
 * Handle an action type command.
 * 
 * Actions commands control the robot moves and are single lettter one.
 * Commands are not blocking, and the returned tx_buffer tells if they have been
 * accepted (not that they are terminated).
 * 
 * Supported ones are:
 * - 'F' : move forward to the next crossing
 * - 'R' : turn right to the next line
 * - 'L' : turn left to the next line
 * - 'S' : stop the ongoing move if any
 * - '!' : panic reset of the application
 */
static bool handle_action_command(char cmde_char) {
#if DEBUG    
    Serial.print(F("# handle_action_command> cmde_char='"));
    Serial.print(cmde_char);
    Serial.print(F("' 0x"));
    Serial.print(cmde_char, HEX);
    Serial.println();
#endif    

    switch (cmde_char) {
        case 'F':
            if (obstacle_distance > 10) { 
                robot.follow_line(100);
                send_event_to_controller(EVENT_MOVING_FORWARD);

                // if we are over a line crossing, delay the line following state
                // enough to be out of the crossing
                if (state == at_crossing) {
                    state = leaving_crossing;
                    // shpw we are in a transient state
                    robot.set_leds(0, 100, 100); 
                    crossing_leaving_delay_reaction = app.delay(500, activate_line_following);
                } else {
                    activate_line_following();
                }

            } else {
                robot.set_leds(100, 0, 0);
                emit_obstacle_sound();
                send_event_to_controller(EVENT_OBSTACLE);

                // abort the current sequence if any
                if (executing_sequence) {
                    #if DEBUG    
                    Serial.println(F("# aborting sequence"));
                    #endif
                    reset_move_sequence();
                }
            }
            break;

        case 'R':
            send_event_to_controller(EVENT_TURNING_RIGHT);

            robot.spin_right(100);
            robot.set_led(LED_RIGHT, 100, 100, 0);
            robot.led_off(LED_LEFT);
            state = turning_stage_1;
            break;

        case 'L':
            send_event_to_controller(EVENT_TURNING_LEFT);
            
            robot.spin_left(100);
            robot.set_led(LED_LEFT, 100, 100, 0);
            robot.led_off(LED_RIGHT);
            state = turning_stage_1;
            break;

        case 'S':
            if (robot.is_moving()) {
                robot.stop();

                // clear the ongoing sequence if any
                reset_move_sequence();

                robot.leds_off();
                // next state depends on where we are now
                state = state_for_current_position();
                // show our mood
                if (state != panic) {
                    notify_readiness();
                } else {
                    notify_panic();
                }
            }   
            break;

        case '!':
            app_reset();
            break;

        case ' ':                   // ignore spaces
            break;

        default:
            move_seq_pc = NULL;     // abort running sequence if any
            robot.set_leds(100, 0, 0);
            emit_error_sound();
            return false;
    }

    *result_value = 0;
    return true;
}

/*
 * Handle a sequence of motion commands
 */
static bool handle_motion_sequence(char *seq) {
#if DEBUG    
    Serial.print(F("# handle_motion_sequence> starting sequence '"));
    Serial.print(seq);
    Serial.println(F("'"));
#endif    
    // initializes the sequence play mechanism and starts the first move
    strcpy(move_seq, seq);                          // save the sequence so that it is not altered by commands received while executing
    move_seq_pc = move_seq;                         // point to the first move
    return handle_action_command(*move_seq_pc);     // execute it
}

/*
 * Command handler 
 * 
 * Delegates the concrete processing to specialized handlers, 
 * based on a first level command parsing.
 */
static void handle_command() {
    bool success = true;

    switch (strlen(rx_buffer)) {
        case 0:
            success = false;
            break;

        case 1:
            success = handle_action_command(rx_buffer[0]);
            break;

        default:
            switch (rx_buffer[0]) {
                case '?':
                    success = handle_query_command(rx_buffer[1]);
                    break;

                case '$':
                    success = handle_motion_sequence(rx_buffer + 1);
                    break;

                default:
                    success = false;
            }
    }

    // send the feedback to the remote peer
    if (success) {
        Serial.write("<OK ");
        Serial.write(rx_buffer);
        if (*result_value) {
            Serial.write(tx_buffer);
        }
    } else {
        Serial.write("<INV ");
        Serial.write(rx_buffer);
    }
    Serial.write('\n');
    Serial.flush();
}

/*
 * Asynchonous handler for data received on the serial link.
 */
static void handle_serial_rx() {
    // get the new byte:
    char inChar = (char)Serial.read();
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n' || inChar == '\r') {
        *cmde_p = 0;
        handle_command();
        clear_command();

    } else {
        // add it to the command buffer if not full
        if (in_buffer < COMMAND_MAX_LEN) {
            *cmde_p++ = inChar;
            in_buffer++;
        }
    }
}

/*
 * The application main
 */
void app_main() {
    // initialize the command and tx_buffer buffer
    memset(rx_buffer, 0, sizeof(rx_buffer));
    cmde_p = rx_buffer;

    memset(tx_buffer, 0, sizeof(tx_buffer));
    tx_buffer[0] = ' ';

    // setup

    Serial.begin(SERIAL_BAUDS);
    app_reset();

    // register the reactions

    //app.onSigUser(SIG_OBSTACLE, obstacle_detected);

    // periodic robot state update (sensors value, motion,...)
    app.onTick(update_application_state);

     // periodic sensor reading publication
    app.repeat(500, stream_sensors_to_controller);

   // serial command handling
    app.onAvailable(&Serial, handle_serial_rx);
}

// Create and start the application reactor
Reactduino app(app_main);
