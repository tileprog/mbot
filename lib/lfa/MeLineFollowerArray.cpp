#include "MeLineFollowerArray.h"

#include "MePort.h"

MeLineFollowerArray::MeLineFollowerArray() : MePort() {
}

MeLineFollowerArray::MeLineFollowerArray(uint8_t port) : MePort(port) {
    // the data line is connected to S2 of the port.
    _pin = s2;
}

uint8_t MeLineFollowerArray::get_value() {
    /*
       Returns the sensor array state as a 6 bits pattern, 1 meaning "above black".

       In case of reading error, 0x80 is returned.
       */
    static uint8_t sensor_data[3];
    unsigned long time_limit = 0;

    // wake the sensor
    pinMode(_pin, OUTPUT);
    digitalWrite(_pin, LOW);
    delayMicroseconds(980);

    // trigger read
    digitalWrite(_pin, HIGH);
    delayMicroseconds(40);

    // get sensor response
    pinMode(_pin, INPUT_PULLUP);
    delayMicroseconds(50); 

    // wait for a high pulse
    time_limit = millis() + 6;
    while ((digitalRead(_pin) == 0) && ((millis() < time_limit))); 
    time_limit = millis() + 6;
    while ((digitalRead(_pin) == 1) && ((millis() < time_limit)));

    // read sensor answer packet (3 bytes with MSB first)
    // Bits are represented by high pulses. 50 < d < 100µs => bit = 1
    for (uint8_t k = 0; k < 3; k++) {
        sensor_data[k] = 0x00;
        for (uint8_t i = 0, mask = 0x80 ; i < 8; i++, mask >>= 1) {
            time_limit = millis() + 6; 
            // wait for rising edge
            while (digitalRead(_pin) == 0 && ((millis() < time_limit)));

            // time the pulse
            uint32_t pulse_us = micros();
            time_limit = millis() + 6; 
            while (digitalRead(_pin) == 1 && ((millis() < time_limit)));
            pulse_us = micros() - pulse_us;

            if (pulse_us > 50 && pulse_us < 100)  {
                sensor_data[k] |= mask;
            }
        }
    }
    if (sensor_data[1] == (uint8_t)(~(uint8_t)sensor_data[0])) {
       return sensor_data[0];
    }
    return LFA_READ_ERROR;
}

