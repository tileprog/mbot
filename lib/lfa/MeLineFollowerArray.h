#ifndef __melinefollowerarray_h__
#define __melinefollowerarray_h__

#include "MeConfig.h" 

#ifdef ME_PORT_DEFINED
#include "MePort.h"
#endif

#define LFA_READ_ERROR  0x80

class MeLineFollowerArray : public MePort {
    private:
        uint8_t _pin;

    public:
        MeLineFollowerArray();
        MeLineFollowerArray(uint8_t port);

        uint8_t get_value();
};

#endif
